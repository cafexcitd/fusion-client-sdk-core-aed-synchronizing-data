<?php require_once('init.php'); ?>
<!DOCTYPE html>
<html>
    <head>
        <title>Fusion Client Core SDK - AED Sample</title>
        
        <style>
            iframe { 
                width: 850px;
                height: 400px; 
            }
        </style>
    </head>
    <body>
        <form id='dataForm'>
            Page URL: <input type='text' id='data'>
            <input type='submit' value='Update'>
        </form>
        <iframe src='about:blank' frameborder='1'></iframe>
    </body>
</html>
<!-- libraries -->
<script type='text/javascript' src='http://fusion.vbox:8080/gateway/adapter.js'></script>
<script type='text/javascript' src='http://fusion.vbox:8080/gateway/fusion-client-sdk.js'></script>
<script src='http://code.jquery.com/jquery-1.10.0.min.js'></script>

<!-- setup Fusion Client SDK -->
<script type='text/javascript'>
    var sessionID = "<?php echo $_SESSION['sessionid']; ?>";
    var urlVersion = -1;
    UC.start(sessionID, []);

    UC.onInitialised = function () {
        var topic = UC.aed.createTopic('aaa');

        // on connecting to the topic, search for the 'url' key
        topic.onConnectSuccess = function (data) {
            for (var i=0; i<data.length; i++) {
                if (data[i].key === 'url') {
                    $('iframe').attr('src', data[i].value);

                    // init the version number
                    urlVersion = data[i].version;
                }
            }
        };

        // failure handler
        topic.onConnectFailed = function (message) {
            alert('Failed to connect to Topic: ' + message);  
        };

        // submit success handler
        topic.onSubmitDataSuccess = function (key, value, version) {
            console.log('Data submitted');
            // no need to add update the UI at this point
            // since the updated data will be sent out to
            // all connected clients - including this one!
            // therefore, we can just wait until the callback
            // to the Topic's onTopicUpdate function.
        };

        // submit failure handler
        topic.onSubmitDataFailed = function (key, value, message) {
            alert('Submit failed!');
        };

        // notification for topic update
        topic.onTopicUpdate = function (key, value, version, deleted) {
            if (key === 'url' && version > urlVersion) {
                $('iframe').attr('src', value);

                // update the version that we've responded to
                urlVersion = version;
            }
        };

        // on submitting the data form - submit data
        $('form#dataForm').submit(function (event) {
            // prevent the default
            event.preventDefault();    

            // with handlers set, submit the data
            var urlString = $('#data').val();
            topic.submitData('url', urlString);    
        });




        // connect to the Topic
        topic.connect();
    };
</script>

