<?php
// start the session so we can access session vars
if (session_id() == '') {
    session_start();
}

$json = '
    {                
        "webAppId": "my-web-app-id123",
        "urlSchemeDetails":
        {
            "host": "fusion.vbox",
            "port": 8443,
            "secure": true
        },
        "aed":
        {
            "accessibleSessionIdRegex": ".*",
            "maxMessageAndUploadSize": "1024",
            "dataAllowance": "10240"
        }
    }
';

// configure the curl options
$ch = curl_init("http://fusion.vbox:8080/gateway/sessions/session");
curl_setopt($ch,CURLOPT_POST, true);
curl_setopt($ch,CURLOPT_POSTFIELDS, $json);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);   
curl_setopt($ch, CURLOPT_HTTPHEADER, array(
    'Content-Type: application/json',
    'Content-Length: ' . strlen($json))                                
);

// execute HTTP POST & close the connection
$response = curl_exec($ch);
curl_close($ch);

// decode the JSON and pick out the ID
$decodedJson = json_decode($response);
$id = $decodedJson->{'sessionid'};

// store the Web Gateway session ID in the HTTP session
$_SESSION['sessionid'] = $id;

?>